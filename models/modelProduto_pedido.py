from banco import db
from config import config
from sqlalchemy.orm import relationship

class Produto_pedido(db.Model):
    __tablename__: 'produto_pedido'
    id = db.Column(db.Integer, primary_key=True)
    produto_id = db.Column(db.Integer, db.ForeignKey('produtos.id'))
    pedido_id = db.Column(db.Integer, db.ForeignKey('pedido.id'))
    quant_compra = db.Column(db.Integer)
    
    produtos = relationship('Produto')
    pedidos = relationship('Pedido')

    def to_json(self):
        json_produto_pedido = {
            'id': self.id,
            'produto_id': self.produto_id,
            'pedido_id': self.pedido_id,
            'quant_compra': self.quant_compra
        }
        return json_produto_pedido

    @staticmethod
    def from_json(json_produto_pedido):
        id = json_produto_pedido.get('id')
        produto_id = json_produto_pedido.get('produto_id')
        pedido_id = json_produto_pedido.get('pedido_id')
        quant_compra = json_produto_pedido.get('quant_compra')

        return Produto_pedido(id=id, produto_id=produto_id, pedido_id=pedido_id, quant_compra=quant_compra)