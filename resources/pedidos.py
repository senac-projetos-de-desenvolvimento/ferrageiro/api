from flask import Blueprint, json, jsonify, request
from banco import db
from models.modelPedido import Pedido
from flask_cors import CORS
from sqlalchemy import desc

pedido = Blueprint('pedido', __name__)

@pedido.route('/pedido', methods=['POST'])
def adicionar():
    pedidos = Pedido.from_json(request.json)
    db.session.add(pedidos)
    db.session.commit()
    return jsonify(pedidos.to_json()), 201

@pedido.route('/pedido', methods=['PUT'])
def alterar():
    pedido = Pedido.query.order_by(Pedido.id).filter(Pedido.id == request.json['id']).one()
    pedido.status = request.json['status']
    pedido.valor_pedido = request.json['valor_pedido']
    db.session.add(pedido)
    db.session.commit()
    return jsonify(pedido.to_json())

@pedido.route('/pedido')
def listagem():
    pedido = Pedido.query.order_by(desc(Pedido.id))
    return jsonify([pedido.to_json() for pedido in pedido])

@pedido.route('/pedidofechados')
def fechado():
    pedido = Pedido.query.order_by(desc(Pedido.id)).filter(Pedido.status == 'fechado').all()
    return jsonify([pedido.to_json() for pedido in pedido])

@pedido.route('/pedidosemaberto')
def aberto():
    pedidos = Pedido.query.order_by(desc(Pedido.id)).filter(Pedido.status == 'aberto').all()
    return jsonify([pedidos.to_json() for pedidos in pedidos])

@pedido.route('/pedidosentregues')
def entregue():
    pedidos = Pedido.query.order_by(desc(Pedido.id)).filter(Pedido.status == 'entregue').all()
    return jsonify([pedidos.to_json() for pedidos in pedidos])

@pedido.route('/pedido/<int:id>')
def status(id):
    pedido = Pedido.query.order_by(Pedido.id).filter(Pedido.id_cliente == id)
    return jsonify([pedido.to_json() for pedido in pedido])

@pedido.route('/pedidos', methods=['POST'])
def addAll():
    for x in request.json:
        pedidos = Pedido.from_json(x)
        db.session.add(pedidos)
        db.session.commit()
        print(pedidos)
    return jsonify([x for x in request.json]), 201