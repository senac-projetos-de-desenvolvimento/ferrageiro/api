from flask import Blueprint, jsonify, request
from flask import json
from flask.json import JSONDecoder
from flask_cors.core import LOG
from banco import db
from models.modelProduto_pedido import Produto_pedido
from flask_cors import CORS

# from models.modelPedido import Pedido

carrinho = Blueprint('produto_pedido', __name__)

@carrinho.route('/carrinho', methods=['POST'])
def adicionar():
    carrinho = Produto_pedido.from_json(request.json)
    db.session.add(carrinho)
    db.session.commit()
    return jsonify(carrinho.to_json()), 201

@carrinho.route('/carrinho', methods=['POST'])
def addAll():
    for x in request.json:
        carrinho = Produto_pedido.from_json(x)
        db.session.add(carrinho)
        db.session.commit()
        print(carrinho)
    return jsonify([x for x in request.json]), 201

@carrinho.route('/carrinho')
def listagem():
    carrinho = Produto_pedido.query.order_by(Produto_pedido.produto_id).all()
    return jsonify([carrinho.to_json() for carrinho in carrinho])

@carrinho.route('/carrinho/<int:id>')
def meucarrinho(id):
    carrinho = Produto_pedido.query.order_by(Produto_pedido.produto_id).filter(Produto_pedido.pedido_id == id)
    return jsonify([carrinho.to_json() for carrinho in carrinho])